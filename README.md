# Supermetrics Code Assigment

__Date__: 2019-08-29
__Author__: Ville Glad <ville.j.glad@gmail.com>

This will fetch the data from API and handles it. Because json is mere a presentational and transitional data structure, handling the data will return PHP primitives like arrays, integers and strings. Only presentational layer will be index.php that triggers and presents the data as json.

This is mainly coded with test driven development method. Due to the constraints of the assigment and time, mainly the happy path is covered by tests.

Some external packages were used: Guzzle to handle http requests, Symfony/Cache to manage caching, and vlucas/dotenv to use environment variables in testing and development.

Development was done by using PHP 7.2.

## Running the json output
```
composer run_output
```
Will return statistics as json. Short caching (50 seconds) is used. It will reduce the calls to external API.

## Running tests
```
composer test_unit // will run only unit tests
composer test_integration // will run only integration tests connected to external web service
composer test // will run all tests ("phpunit")
```
Note: There are integration tests that hits the Supermetrics API. Caching added for convenient repeated calls to API.
Tests are using group annotations to define whether they are unit or integration tests.

## Running in a browser
```
composer serve
```
Will run PHP in-built local server on localhost:8000 to see the results. Serves public-directory.

## Environment variables

Copy .env.dist to .env and fill out the variables for testing.
