<?php
declare(strict_types=1);

use GuzzleHttp\Client;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use villeglad\Supermetrics\ApiAdapter;
use villeglad\Supermetrics\PostStats;

require_once(__DIR__ . '/bootstrap.php');

header('Content-Type: application/json');

$client = new Client();
$cache = new FilesystemAdapter('dev', 0, __DIR__ . '/../var/cache');

$api = new ApiAdapter($client, $cache, getenv('TEST_CLIENT_ID'), getenv('TEST_EMAIL'), getenv('TEST_NAME'));
$posts = $api->fetchPosts();

$postStats = new PostStats();
$postStats->addPosts($posts['posts']);

echo json_encode($postStats->getAllStats());
