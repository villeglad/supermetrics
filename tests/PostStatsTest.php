<?php
declare(strict_types=1);

namespace villeglad\Supermetrics\Tests;

use PHPUnit\Framework\TestCase;
use villeglad\Supermetrics\PostStats;
use villeglad\Supermetrics\Post;

/**
 * @group unit
 */
final class PostStatsTest extends TestCase
{

    public function testExistingIdOfPostWillBeSkippedOnAddingPosts(): void
    {
        $testData = $this->exampleData();
        $existingPostId = new Post([
            'id' => 'post12345',
            'from_name' => 'First Last',
            'from_id' => 'user_1',
            'message' => 'Lorem ipsum dolor sit amet',
            'type' => 'status',
            'created_time' => '2019-07-22T10:00:50+00:00'
        ]);

        $testData[] = $existingPostId;

        $postStats = new PostStats();
        $postStats->addPosts($testData);

        $this->assertCount(3, $postStats->getPosts());
    }

    public function testAverageCharacterLengthPerPostPerMonth(): void
    {
        $postStats = new PostStats();
        $postStats->addPosts($this->exampleData());

        $monthlyAverageCharacterLengths = $postStats->getMonthlyAverageCharacterLengths();

        $this->assertEquals(78, $monthlyAverageCharacterLengths['2019-08']);
        $this->assertArrayHasKey('2019-07', $monthlyAverageCharacterLengths);
        $this->assertArrayHasKey('2019-08', $monthlyAverageCharacterLengths);
    }

    public function testLongestPostByCharacterLengthPerMonth(): void
    {
        $postStats = new PostStats();
        $postStats->addPosts($this->exampleData());

        $longestPosts = $postStats->getLongestPostByCharacterLengthPerMonth();

        $this->assertSame('post12346', $longestPosts['2019-07']['id']);
        $this->assertSame('post12347', $longestPosts['2019-08']['id']);

    }

    public function testTotalPostsSplitByWeek(): void
    {
        $postStats = new PostStats();
        $postStats->addPosts($this->exampleData());

        $weeks = $postStats->getNumberOfPostsByWeeks();

        $this->assertSame(2, $weeks['2019-35']);
        $this->assertSame(1, $weeks['2019-30']);
    }

    public function testAverageNumberOfPostsPerMonthForAUser(): void
    {
        $postStats = new PostStats();
        $postStats->addPosts($this->exampleData());

        $postsPerUser = $postStats->getAverageNumberOfPostsPerMonthForAUser();

        $this->assertSame(2, $postsPerUser);
    }

    public function testAllStatsHasDesiredData(): void
    {
        $postStats = new PostStats();
        $postStats->addPosts($this->exampleData());

        $allStats = $postStats->getAllStats();

        $this->assertArrayHasKey('numberOfPostsByWeeks', $allStats);
        $this->assertArrayHasKey('longestPostPerMonth', $allStats);
        $this->assertArrayHasKey('monthlyAverageLengths', $allStats);
        $this->assertArrayHasKey('averageNumberOfPostsPerUser', $allStats);
    }

    private function exampleData(): array
    {
        $post1 = new Post([
            'id' => 'post12345',
            'from_name' => 'First Last',
            'from_id' => 'user_1',
            'message' => 'Lorem ipsum dolor sit amet xxx',
            'type' => 'status',
            'created_time' => '2019-08-27T10:00:50+00:00'
        ]);
        $post2 = new Post([
            'id' => 'post12346',
            'from_name' => 'First Last',
            'from_id' => 'user_1',
            'message' => 'Lorem ipsum dolor sit amet',
            'type' => 'status',
            'created_time' => '2019-07-22T10:00:50+00:00'
        ]);
        $post3 = new Post([
            'id' => 'post12347',
            'from_name' => 'First Last',
            'from_id' => 'user_2',
            'message' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. '
                . 'Aenean condimentum nunc eget ipsum dictum, a euismod ipsum facilisis.',
            'type' => 'status',
            'created_time' => '2019-08-26T10:00:50+00:00'
        ]);

        return [$post1, $post2, $post3];
    }
}
