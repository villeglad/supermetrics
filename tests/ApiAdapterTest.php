<?php
declare(strict_types=1);

namespace villeglad\Supermetrics\Tests;

use GuzzleHttp\Client;
use villeglad\Supermetrics\ApiAdapter;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;

/**
 * @group integration
 */
final class ApiAdapterTest extends TestCase
{
    private const CACHE_DIR = __DIR__ . '/../var/cache';
    private const CACHE_NAMESPACE = 'tests';

    private $cache;

    protected function setUp(): void
    {
        parent::setUp();
        $this->cache = new FilesystemAdapter(self::CACHE_NAMESPACE, 0, self::CACHE_DIR);
    }

    public function testPostsAreRetrieved(): void
    {
        $client = new Client();

        $apiAdapter = new ApiAdapter(
            $client,
            $this->cache,
            getenv('TEST_CLIENT_ID'),
            getenv('TEST_EMAIL'),
            getenv('TEST_NAME')
        );

        $posts = $apiAdapter->fetchPosts(10);
        $this->assertCount(10, $posts['pagesFetched']);
        $this->assertNotEmpty($posts['posts']);
    }
}
