<?php
declare(strict_types=1);

namespace villeglad\Supermetrics\Tests;

use DateTimeImmutable;
use RuntimeException;
use PHPUnit\Framework\TestCase;
use villeglad\Supermetrics\Post;

/**
 * @group unit
 */
final class PostTest extends TestCase
{
    public function testImmutablePostIsCreated(): void
    {
        $testData = [
            'id' => 'post12345',
            'from_name' => 'First Last',
            'from_id' => 'user_1',
            'message' => 'Lorem ipsum dolor sit amet',
            'type' => 'status',
            'created_time' => $time = '2019-08-27T10:00:50+00:00'
        ];
        $post = new Post($testData);

        $this->assertSame('post12345', $post->getId());
        $this->assertSame('First Last', $post->getFromName());
        $this->assertSame('user_1', $post->getFromId());
        $this->assertSame('Lorem ipsum dolor sit amet', $post->getMessage());
        $this->assertSame('status', $post->getType());
        $this->assertSame('2019-08', $post->getMonth());
        $this->assertSame('2019-35', $post->getWeek());
        $this->assertEquals(new DateTimeImmutable($time), $post->getCreatedTime());
    }

    public function testPostCannotBeCreatedWithoutTimestamp(): void
    {
        $invalidTestData = [
            'id' => 'post12345',
            'from_name' => 'First Last',
            'from_id' => 'user_1',
            'message' => 'Lorem ipsum dolor sit amet',
            'type' => 'status',
        ];

        $this->expectException(RuntimeException::class);
        new Post($invalidTestData);
    }
}
