<?php
declare(strict_types=1);

namespace villeglad\Supermetrics;

final class PostStats
{
    private $posts;

    public function __construct()
    {
        $this->posts = [];
    }

    public function getPosts(): array
    {
        return $this->posts;
    }

    public function addPosts(array $posts): void
    {
        foreach ($posts as $post) {
            if (array_key_exists($post->getId(), $this->posts)) {
                continue;
            }
            $this->posts[$post->getId()] = $post;
        }
    }

    public function getMonthlyAverageCharacterLengths(): array
    {
        $monthlyPosts = $this->getMonthlyPosts();

        $data = [];

        foreach ($monthlyPosts as $month => $posts) {
            $totalLength = array_reduce($posts, function (int $carry, Post $post): int {
                return $carry + $post->getMessageLength();
            }, 0);
            $data[$month] = round($totalLength / count($posts));
        }

        return $data;
    }

    public function getLongestPostByCharacterLengthPerMonth(): array
    {
        $monthlyPosts = $this->getMonthlyPosts();

        $data = [];

        foreach ($monthlyPosts as $month => $posts) {
            $topPost = null;
            foreach ($posts as $post) {
                $topPost = $topPost === null || $post->getMessageLength() > $topPost->getMessageLength()
                    ? $post
                    : $topPost;
            }
            $data[$month] = $topPost->toArray();
        }

        return $data;
    }

    public function getNumberOfPostsByWeeks(): array
    {
        $weeklyPosts = [];
        foreach ($this->posts as $post) {
            $yearWeek = $post->getWeek();
            $currentCount = array_key_exists($yearWeek, $weeklyPosts) ? $weeklyPosts[$yearWeek] : 0;
            $weeklyPosts[$yearWeek] = $currentCount + 1;
        }

        return $weeklyPosts;
    }

    public function getAllStats(): array
    {
        return [
            'numberOfPostsByWeeks' => $this->getNumberOfPostsByWeeks(),
            'longestPostPerMonth' => $this->getLongestPostByCharacterLengthPerMonth(),
            'monthlyAverageLengths' => $this->getMonthlyAverageCharacterLengths(),
            'averageNumberOfPostsPerUser' => $this->getAverageNumberOfPostsPerMonthForAUser(),
        ];
    }

    public function getAverageNumberOfPostsPerMonthForAUser(): int
    {
        return (int) round($this->postCount() / $this->getNumberOfMonths());
    }

    private function getMonthlyPosts(): array
    {
        $monthlyPosts = [];

        foreach ($this->posts as $post) {
            $monthlyPosts[$post->getMonth()][] = $post;
        }

        return $monthlyPosts;
    }

    private function getNumberOfMonths(): int
    {
        return count(array_keys($this->getMonthlyPosts()));
    }

    private function postCount(): int
    {
        return count($this->posts);
    }
}
