<?php
declare(strict_types=1);

namespace villeglad\Supermetrics;

use DateTimeImmutable;
use Exception;
use RuntimeException;

final class Post
{
    private $id;
    private $fromName;
    private $fromId;
    private $message;
    private $type;
    private $createdTime;

    public function __construct(array $postArray)
    {
        $this->id = array_key_exists('id', $postArray) ? $postArray['id'] : '';
        $this->fromName = array_key_exists('from_name', $postArray) ? $postArray['from_name'] : '';
        $this->fromId = array_key_exists('from_id', $postArray) ? $postArray['from_id'] : '';
        $this->message = array_key_exists('message', $postArray) ? $postArray['message'] : '';
        $this->type = array_key_exists('type', $postArray) ? $postArray['type'] : '';

        try {
            $this->createdTime = new DateTimeImmutable($postArray['created_time']);
        } catch (Exception $e) {
            throw new RuntimeException('Creation time missing on: ' . json_encode($postArray));
        }
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getFromName(): string
    {
        return $this->fromName;
    }

    public function getFromId(): string
    {
        return $this->fromId;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getCreatedTime(): DateTimeImmutable
    {
        return $this->createdTime;
    }

    public function getMessageLength(): int
    {
        return strlen($this->getMessage());
    }

    public function getMonth(): string
    {
        return $this->getCreatedTime()->format('Y-m');
    }

    public function getWeek(): string
    {
        return $this->getCreatedTime()->format('Y-W');
    }

    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'fromId' => $this->getFromId(),
            'fromName' => $this->getFromName(),
            'message' => $this->getMessage(),
            'type' => $this->getType(),
            'createdTime' => $this->getCreatedTime()->format('Y-m-d H:i:s'),
            'messageLength' => $this->getMessageLength(),
        ];
    }
}
