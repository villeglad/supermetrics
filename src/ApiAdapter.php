<?php
declare(strict_types=1);

namespace villeglad\Supermetrics;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Contracts\Cache\CacheInterface;
use RuntimeException;

final class ApiAdapter
{
    private const TOKEN_CACHE_EXPIRATION = 50;
    private const POST_CACHE_EXPIRATION = 600;

    private $client;
    private $cache;
    private $clientId;
    private $email;
    private $name;
    private $token;

    public function __construct(
        ClientInterface $client,
        CacheInterface $cache,
        string $clientId,
        string $email,
        string $name
    ) {
        $this->client = $client;
        $this->cache = $cache;
        $this->clientId = $clientId;
        $this->email = $email;
        $this->name = $name;
    }

    public function fetchPosts($numberOfPages = 10): array
    {
        $queriedPosts = [
            'pagesFetched' => [],
            'posts' => []
        ];

        foreach (range(1, $numberOfPages) as $page) {
            $query = $this->fetchPost($page);
            $queriedPosts['pagesFetched'][] = $page;
            foreach ($query['posts'] as $post) {
                $queriedPosts['posts'][] = $post;
            }
        }

        return $queriedPosts;
    }

    private function retrieveToken(): void
    {
        $cachedToken = $this->cache->getItem('posts.' . $this->clientId . '.sl_token');

        if ($cachedToken->isHit()) {
            $this->token = $cachedToken->get();
            return;
        }

        $response = $this->client->request('POST', 'https://api.supermetrics.com/assignment/register', [
            'form_params' => [
                'client_id' => $this->clientId,
                'email' => $this->email,
                'name' => $this->name,
            ]
        ]);

        $body = $response->getBody();
        $contents = json_decode($body->getContents(), true);
        $this->token = $contents['data']['sl_token'];

        $cachedToken->set($this->token);
        $cachedToken->expiresAfter(self::TOKEN_CACHE_EXPIRATION);
        $this->cache->save($cachedToken);
    }

    private function fetchPost($pageNumber = 1): array
    {
        $this->retrieveToken();

        $postsByPage = $this->cache->getItem('posts.' . $this->clientId . '.by_page_' . $pageNumber);

        if ($postsByPage->isHit()) {
            return $postsByPage->get();
        }

        try {
            $response = $this->client->request('GET', 'https://api.supermetrics.com/assignment/posts', [
                'query' => [
                    'sl_token' => $this->token,
                    'page' => $pageNumber,
                ]
            ]);

            $contents = json_decode($response->getBody()->getContents(), true);

            $posts = array_map(function (array $item): Post {
                return new Post($item);
            }, $contents['data']['posts']);

            $result = [
                'posts' => $posts,
                'pageNumber' => $contents['data']['page'],
            ];

            $postsByPage->set($result);
            $postsByPage->expiresAfter(self::POST_CACHE_EXPIRATION);
            $this->cache->save($postsByPage);

            return $result;
        } catch (GuzzleException $e) {
            throw new RuntimeException($e->getMessage());
        }
    }
}
